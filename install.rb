#!/usr/bin/env ruby
require "rbconfig"
require "ftools"
include Config

RV = File.join(CONFIG['sitedir'], CONFIG['MAJOR'] + '.' + CONFIG['MINOR'])
DSTPATH = RV

FILES = %w(mp3tag.rb)
MANFILES = %(mp3tag.3)

File.mkpath DSTPATH, true

begin
  FILES.each { |name|
    File.install name, File.join(DSTPATH), 0644, true
  }
  MANFILES.each {
    |name|
    File.install name, File.join(CONFIG['mandir'], 'man3'), 0644, true
  }
rescue
  puts 'install failed!'
  puts $!
else
  puts 'install succeed!'
end
