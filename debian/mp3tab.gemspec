Gem::Specification::new do |spec|

 spec.name = "mp3tag"
  spec.version = "2.6.0"
  spec.platform = Gem::Platform::RUBY
  spec.summary = "mp3tag"
  spec.description = "Ruby library for manipulating ID3V1.1 tags in MP3"
  spec.license = "GPL2"

  spec.files = [ "lib/mp3tag.rb" ]

  spec.executables = []
  
  spec.require_path = "lib"

  spec.test_files = nil

### spec.add_dependency 'lib', '>= version'
#### spec.add_dependency 'map'

  spec.extensions.push(*[])

  spec.author = "Lars Christensen"
  spec.email = "dsl8950@vip.cybercity.dk"
  spec.homepage = "http://users.cybercity.dk/~dsl8950/ruby/mp3tag.html"
end
